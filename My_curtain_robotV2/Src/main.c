/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "rtc.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "opt3001.h"
#include "wakeup.h"
#include "my_string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    /* USER CODE BEGIN 1 */
    uint16_t ID = 0;
    uint8_t recv_dat[6] = {0};
    uint8_t Find_flag =0;
    uint8_t Time_buf[20];
    uint32_t start_time,end_time,worktime=0;
    /* USER CODE END 1 */


    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */

    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_USART1_UART_Init();
    MX_USART2_UART_Init();
    MX_I2C1_Init();
    MX_RTC_Init();
    MX_ADC_Init();
    /* USER CODE BEGIN 2 */
    printf("################\r\n");
    EnableUsart_IT();
//	OPT_Read_ID(recv_dat);

//蓝牙工作状态
    HAL_GPIO_WritePin(BT_EN_GPIO_Port, BT_EN_Pin, GPIO_PIN_SET);         //下降沿唤醒 1-->0
    HAL_Delay(1000);
    HAL_GPIO_WritePin(BT_EN_GPIO_Port, BT_EN_Pin, GPIO_PIN_RESET);
    HAL_Delay(1000);   //10秒时间 手机APP反复搜索，都可以搜索到！！！
    printf("will deep sleep\r\n");
    time2Stamp();
//蓝牙休眠状态      手机APP反复搜索，搜索不到！！！
    HAL_GPIO_WritePin(BT_EN_GPIO_Port, BT_EN_Pin, GPIO_PIN_RESET);       //上升沿休眠  0-->1
    HAL_Delay(1000);
    HAL_GPIO_WritePin(BT_EN_GPIO_Port, BT_EN_Pin, GPIO_PIN_SET);
    HAL_Delay(1000);
//  测试低功耗：
    //RTC_Time_Config(10000); //等换了外部晶振再测试定时这功能吧
    SystemPower_Config();   //配置低功耗模式（GPIO相关）
    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI); //进入STOP低功耗模式
    SystemClockConfig_STOP(); //恢复外设时钟
    printf("WearkUP------>\r\n");
    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    while (1)
    {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
//        printf("SEND------>\r\n");
        if(Usart2type.UsartRecFlag ==1)
        {
            Usart2type.UsartRecFlag = 0;
            if( strstr((char *)Usart2type.Usart2RecBuffer,"ON") != NULL ) //开启指令
            {
                memset(Usart2type.Usart2RecBuffer, 0x00, sizeof(Usart2type.Usart2RecBuffer)); //先清空缓冲区
                printf("@@@@@@@@@@@@@@@@@\r\n");
                if(worktime != 0)  //已配置时间
                {
                    HAL_GPIO_WritePin(GPIOB, NSLEEP_Pin, GPIO_PIN_SET);  //关闭驱动休眠
                    HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_SET);   //正向
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_RESET);
					HAL_Delay(worktime); //运行时间
					HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_SET);   //刹车
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_SET);
                    HAL_GPIO_WritePin(GPIOB, NSLEEP_Pin, GPIO_PIN_RESET);  //驱动进入休眠
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_RESET);   //关闭输出 降低功耗
                    HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_RESET);   //关闭输出 降低功耗
                }
            }
            if( strstr((char *)Usart2type.Usart2RecBuffer,"OFF") != NULL ) //关闭指令
            {
                printf("&&&&&&&&&&&&&&&&&&\r\n");
                memset(Usart2type.Usart2RecBuffer, 0x00, sizeof(Usart2type.Usart2RecBuffer)); //先清空缓冲区
                if(worktime != 0)  //已配置时间
                {
                   HAL_GPIO_WritePin(GPIOB, NSLEEP_Pin, GPIO_PIN_SET);   //关闭驱动休眠
                    HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_RESET);   //反向向
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_SET);
					HAL_Delay(worktime); //运行时间
					HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_SET);   //刹车
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_SET);
                    HAL_GPIO_WritePin(GPIOB, NSLEEP_Pin, GPIO_PIN_RESET);  //驱动进入休眠
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_RESET);   //关闭输出 降低功耗
                    HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_RESET);   //关闭输出 降低功耗
                    //MCU 进入休眠
                    time2Stamp();
                    SystemPower_Config();   //配置低功耗模式（GPIO相关）
                    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI); //进入STOP低功耗模式
                }
            }
            if( strstr((char *)Usart2type.Usart2RecBuffer,"S") != NULL ) //初始化时钟指令
            {
                if( strstr((char *)Usart2type.Usart2RecBuffer,"T") != NULL )
                {
                    memset(Time_buf, 0x00, sizeof(Time_buf)); //先清空缓冲区
                    Find_flag =  Find_string((char *)Usart2type.Usart2RecBuffer,"S","T",(char *)Time_buf);
                    printf("time: %s,%d",Time_buf,(uint32_t )atoi((char *)Time_buf));
                    if(Find_flag) {
                        Calibration_Times((char *)Time_buf);
                    }
                }
                memset(Usart2type.Usart2RecBuffer, 0x00, sizeof(Usart2type.Usart2RecBuffer)); //先清空缓冲区
            }
            // HAL_GetTick();    //获取系统运行时间   uint32_t
            if( strstr((char *)Usart2type.Usart2RecBuffer,"C") != NULL ) //初始化时钟指令
            {
                if(start_time == 0) //第一次初始化
                {
                    start_time = HAL_GetTick();
                    HAL_GPIO_WritePin(GPIOB, NSLEEP_Pin, GPIO_PIN_SET);  //关闭驱动休眠
//                HAL_Delay(500);
                    HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_SET);   //正向
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_RESET);
                } else
                {
                    HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_SET);   //刹车
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_SET);

                    HAL_GPIO_WritePin(GPIOB, NSLEEP_Pin, GPIO_PIN_RESET);  //驱动进入休眠
                    HAL_GPIO_WritePin(GPIOB, N_IN2_Pin, GPIO_PIN_RESET);   //关闭输出 降低功耗
                    HAL_GPIO_WritePin(GPIOB, N_IN1_Pin, GPIO_PIN_RESET);   //关闭输出 降低功耗
                    end_time = HAL_GetTick();
                    worktime = end_time-start_time;
                    start_time = 0;
                    end_time = 0;
                    printf("worktime:%d\r\n",worktime);
                }
                memset(Usart2type.Usart2RecBuffer, 0x00, sizeof(Usart2type.Usart2RecBuffer)); //先清空缓冲区
            }


        }
        HAL_Delay(50);
    }
    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

    /** Configure the main internal regulator output voltage
    */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
        Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                                         |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_RTC;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
    PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
    PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */





/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
